<?php get_header(); ?>

      <section class="last-posts">
        <div class="container">

          <div class="row">
            <div class="col-md-8 col-md-offset-2">

            <?php if (have_posts()): while (have_posts()) : the_post(); ?>

              <article id="post-<?php the_ID(); ?>" class="post-home col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <h2 class="last-posts-title">
                  <a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>"> <?php the_title(); ?> </a>
                </h2>

                <a href="<?php the_permalink(); ?>"> <?php the_post_thumbnail( 'home-thumb', array('class' => 'last-posts-th col-xs-12 col-sm-3 col-md-3 col-lg-3')); ?></a>

                <h5 class="post-datetime">
                  <?php the_time('j \d\e F \d\e Y'); ?>
                </h5>
                <section class="last-posts-body">
                  <?php the_excerpt(); ?>
                </section>

                <div class="post-divider">
                  <a href="<?php the_permalink(); ?>">Leia Mais...</a>
                </div>
              </article>

            <?php endwhile; ?>

            </div>
          </div>

        </div>
      </section>

      <section class="video-slider">
        <div class="container">

          <div class="row">
            <div class="col-md-8 col-md-offset-2 social-contacts">
          <?php wp_pagenavi(); ?>
          <?php endif; ?>
            </div>
          </div>

        </div>
      </section>

<?php get_footer(); ?>
