<?php get_header(); ?>


    <section class="">
      <div class="container">

        <?php if (have_posts()) : while (have_posts()) : the_post(); ?>

        <article class="row">
          <div class="col-md-8 col-md-offset-2">

            <header class="row">
              <div class="col-lg-12">
                <h1 class="page-title">
                <?php the_title(); ?>
                </h1>

                <h5 class="post-datetime-single">
                  <?php the_time('j \d\e F \d\e Y'); ?>
                  •
                  por <?php the_author(); ?>
                </h5>

              </div>
            </header>


            <div class="row col-lg-12 clearfix">
              <?php the_content(); ?>
            </div>

            <h2 class="pull-left">Compartilhe </h2>

            <section class="row col-lg-12 clearfix">

              <div class="text-center social-bar social-contacts">
              <?php if ( function_exists( 'sharing_display' ) ) {
                  sharing_display( '', true );
                }

              if ( class_exists( 'Jetpack_Likes' ) ) {
                  $custom_likes = new Jetpack_Likes;
                  echo $custom_likes->post_likes( '' );
                }
              ?>
              </div>

            </section>

          </div>
        </article>

      </div><!-- end .container -->

      <section class="comments-section">
        <div class="container">

          <div class="row">
            <div class="col-md-8 col-md-offset-2">

              <div class="clearfix">
                <h2 class="pull-left">Comentários</h2>
              </div>

              <?php comments_template(); ?>

            </div>
          </div>

        </div>
      </section>

        <?php endwhile; ?>
        <?php else : ?>
        <?php endif; ?>

    </section>


<?php get_footer(); ?>
