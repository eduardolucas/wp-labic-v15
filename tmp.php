




























// Search Form
function labicv15_wpsearch($form) {
	$form = '<form role="search" method="get" action="' . home_url( '/' ) . '" >
  	<label class="screen-reader-text" for="s">' . __('', 'labicv15') . '</label>
  	<input class="sb-search-input" type="text" value="' . get_search_query() . '" name="search" id="search" placeholder="Buscar" />
  	<input class="sb-search-submit" type="submit" value="'. esc_attr__('Search') .'" />
    <span class="sb-icon-search"></span>
	</form>';
	return $form;
}
