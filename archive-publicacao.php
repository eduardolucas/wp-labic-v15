<?php get_header(); ?>

<nav id="navbar-publ" class="navbar navbar-default navbar-fixed-publ">
      <div class="container">
        <div class="navbar-header">
          <button class="navbar-toggle collapsed" type="button" data-toggle="collapse" data-target=".bs-example-js-navbar-scrollspy">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="#"><span class="publ-title">Publicações</span></a>
        </div>
        <div class="collapse navbar-collapse bs-example-js-navbar-scrollspy">
          <ul class="nav navbar-nav navbar-left">
            <li><a id="#trabalhos" href="#trabalhos"><span class="glyphicon glyphicon-book"></span> Trabalhos completos</a></li>
            <li><a id="#tcc" href="#tcc"><span class="glyphicon glyphicon-book"></span> TCCs</a></li>
          </ul>
        </div>
      </div>
    </nav>


      <header class="header-publ">
        <div class="container">

          <div class="row">
            <div class="col-md-8 col-md-offset-2">

              <div class="row">
                <div class="col-lg-12">

                  <article class="page-excerpt">
                      <!-- intro texto publicações -->
                  </article>

                </div>
              </div>

            </div>
          </div>

        </div>
        <!-- container -->
      </header>

      <div class="container container-publ">

                <div data-spy="scroll" data-target="#navbar-publ" data-offset="0" class="scrollspy-publ">


                  <br><br>

                  <p id="trabalhos"></p>
                  <h3 class="publ-divider"><a><span class="glyphicon glyphicon-book"></span> Trabalhos Completos</a></h3>

                  <table class="table table-striped">

                        <thead>
                          <tr>
                              <th></th>
                              <th>Título</th>
                              <th>Autor</th>
                              <th>Ano</th>
                              <th>Download</th>
                          </tr>
                        </thead>

                        <tbody>

                          <?php
                              $args = array(
                                'post_type' => 'publicacao',
                                'posts_per_page' => '50',
                                'tax_query' => array(
                                  array(
                                    'taxonomy' => 'categoria-de-publicacao',
                                    'field' => 'slug',
                                    'terms' => 'trabalhos'
                                  )
                                )
                              );
                              $publicacoes = new WP_Query( $args );
                              if( $publicacoes->have_posts() ) {
                                while( $publicacoes->have_posts() ) {
                                  $publicacoes->the_post();
                                  ?>

                                  <tr>
                                    <td>
                                      <?php the_post_thumbnail( 'publ-thumb', array('class' => 'img-thumbnail')); ?>
                                    </td>
                                    <td>
                                      <?php the_title() ?>
                                    </td>
                                    <td><p>
                                      <?php echo get_post_meta($post->ID, 'wpcf-autor-publ', TRUE); ?></p>
                                    </td>
                                    <td>
                                      <!-- post meta ano -->
                                      <?php echo get_post_meta($post->ID, 'wpcf-ano-publ', TRUE); ?>
                                    </td>
                                    <td>
                                      <a target="_blank" href="<?php echo get_post_meta($post->ID, 'wpcf-url-publ', TRUE); ?>"><div class="file-icon file-icon-default" data-type="<?php echo get_post_meta($post->ID, 'wpcf-file-type', TRUE); ?>"></div></a>
                                    </td>
                                  </tr>

                          <?php
                        }
                      }
                      else {
                        echo '';
                      }
                    ?>

                        </tbody>

                  </table>

                  <br><br>

                  <p id="tcc"></p>
                  <h3 class="publ-divider"><a><span class="glyphicon glyphicon-book"></span> Trabalhos de Conclusão de Curso</a></h3>

                  <table class="table table-striped">

                        <thead>
                          <tr>
                              <th></th>
                              <th>Título</th>
                              <th>Autor</th>
                              <th>Ano</th>
                              <th>Download</th>
                          </tr>
                        </thead>

                        <tbody>

                          <?php
                              $args = array(
                                'post_type' => 'publicacao',
                                'posts_per_page' => '30',
                                'tax_query' => array(
                                  array(
                                    'taxonomy' => 'categoria-de-publicacao',
                                    'field' => 'slug',
                                    'terms' => 'tcc'
                                  )
                                )
                              );
                              $publicacoes = new WP_Query( $args );
                              if( $publicacoes->have_posts() ) {
                                while( $publicacoes->have_posts() ) {
                                  $publicacoes->the_post();
                                  ?>

                                  <tr>
                                    <td>
                                      <?php the_post_thumbnail( 'publ-thumb', array('class' => 'img-thumbnail')); ?>
                                    </td>
                                    <td>
                                      <?php the_title() ?>
                                    </td>
                                    <td><p>
                                      <?php echo get_post_meta($post->ID, 'wpcf-autor-publ', TRUE); ?></p>
                                    </td>
                                    <td>
                                      <!-- post meta ano -->
                                      <?php echo get_post_meta($post->ID, 'wpcf-ano-publ', TRUE); ?>
                                    </td>
                                    <td>
                                      <a target="_blank" href="<?php echo get_post_meta($post->ID, 'wpcf-url-publ', TRUE); ?>"><div class="file-icon file-icon-default" data-type="<?php echo get_post_meta($post->ID, 'wpcf-file-type', TRUE); ?>"></div></a>
                                    </td>
                                  </tr>

                          <?php
                        }
                      }
                      else {
                        echo '';
                      }
                    ?>

                        </tbody>

                  </table>

                </div>

        </div>


        <section class="video-slider">
          <div class="container">

            <div class="row">
              <div class="col-md-8 col-md-offset-2">

                <div class="row">
                  <div class="col-lg-12">
                    <h1 class="video-slider-title pull-left">Vídeos</h1>
                  </div>
                </div>


                <div class="row">
                  <div class="col-lg-12">

                    <?php

                      $paged = (get_query_var('paged')) ? get_query_var('paged') : 1;

                      query_posts('post_type=video&categoria-de-video=publicacoes&posts_per_page=6&paged='.$paged); //set your own query here

                      get_template_part( 'loop', 'grid' );

                      wp_reset_query();
                    ?>

                  </div>
                </div>

              </div>
            </div>

        </div>
        </section>


<?php get_footer(); ?>
