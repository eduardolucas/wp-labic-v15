<?php /* Template Name: Contact Page */ get_header(); ?>

      <section class="">
        <div class="container">

          <div class="row">
            <div class="col-md-8 col-md-offset-2">

              <div class="row">
                <div class="col-lg-12">
                  <h1 class="page-title">
                    <?php the_title(); ?>
                  </h1>

                </div>
              </div>


              <div class="row">
                <div class="col-lg-12">

                  <div class="contact-form clearfix">

                    <?php if (have_posts()): while (have_posts()) : the_post(); ?>

                			<!-- article -->
                			<article id="post-<?php the_ID(); ?>"
                        <?php post_class(); ?>>

                				<?php the_content(); ?>

                			</article>
                			<!-- /article -->

                		<?php endwhile; ?>
                		<?php endif; ?>

                  </div>


                  <div class="social-bar">
                    <address>Laboratório de Estudos sobre Imagem e Cibercultura -  Labic
                    <br>
                    <br>UNIVERSIDADE FEDERAL DO ESPÍRITO SANTO

                    <br><h5>CENTRO DE ARTES - DEPARTAMENTO DE COMUNICAÇÃO SOCIAL

                    <br><br>Cemuni I, sala 10 Telefone: (27) 4009 2752</h5>
                    </address>
                  </div>

                  <div class="social-bar social-contacts">
                    <a target="_blank" href="https://github.com/labic"> <img width="32" src="<?php echo get_stylesheet_directory_uri(); ?>/assets/github10.png" alt="" /> </a>
                    <a target="_blank" href="http://twitter.com/ufeslabic"> <img width="32" src="<?php echo get_stylesheet_directory_uri(); ?>/assets/twitter.png" alt="" /> </a>
                    <a target="_blank" href="https://www.facebook.com/labic.ufes"> <img width="32" src="<?php echo get_stylesheet_directory_uri(); ?>/assets/facebook.png" alt="" /> </a>

                  </div>

                </div>
              </div>

            </div>
          </div>

        </div>
      </section>

      <div>
      <iframe frameborder="0" scrolling="no" width="100%" height="360" src="http://www.openstreetmap.org/export/embed.html?bbox=-40.31688451766968%2C-20.28651204195925%2C-40.2913498878479%2C-20.27204029432185&amp;layer=mapnik&amp;marker=-20.27927633696833%2C-40.30411720275879">
      </iframe>
      </div>

<?php get_footer(); ?>
