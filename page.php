<?php get_header(); ?>

      <section class="">
        <div class="container">

          <div class="row">
            <div class="col-md-8 col-md-offset-2">

              <div class="row">
              <div class="col-lg-12">
                <h1 class="page-title">
                <?php the_title(); ?>
                </h1>
              </div>
              </div>


              <div class="row">
              <div class="col-lg-12">
              <div class="contact-form clearfix">

                  <?php if (have_posts()): while (have_posts()) : the_post(); ?>
                  <!-- article -->
                  <article id="post-<?php the_ID(); ?>"
                  <?php post_class(); ?>>
                  <?php the_content(); ?>
                  </article>
                  <!-- /article -->
                  <?php endwhile; ?>
                  <?php endif; ?>

              </div>
              </div>
              </div>

            </div>
          </div>

        </div>
      </section>

<?php get_footer(); ?>
