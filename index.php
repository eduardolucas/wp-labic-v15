<?php get_header(); ?>

      <section class="featured-tiles">

        <!-- wp tiles -->

      </section>

      <section class="last-posts">
        <div class="container">

          <div class="row">
            <div class="col-md-8 col-md-offset-2">

            <?php if (have_posts()): while (have_posts()) : the_post(); ?>

              <article id="post-<?php the_ID(); ?>" class="post-home col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <h2 class="last-posts-title">
                  <a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>"> <?php the_title(); ?> </a>
                </h2>

                <a href="<?php the_permalink(); ?>"> <?php the_post_thumbnail( 'home-thumb', array('class' => 'last-posts-th col-xs-5 col-sm-3 col-md-3 col-lg-3')); ?></a>

                <h5 class="post-datetime">
                  <?php the_time('j \d\e F \d\e Y'); ?>
                </h5>
                <section class="last-posts-body">
                  <?php the_excerpt(); ?>
                </section>

                <div class="post-divider">
                  <a href="<?php the_permalink(); ?>">Leia Mais...</a>
                </div>
              </article>

            <?php endwhile; ?>
            <?php endif; ?>

            </div>
          </div>

        </div>
      </section>

      <section class="video-slider">
        <div class="container">

          <div class="row">
            <div class="col-md-8 col-md-offset-2">

              <div class="row">
                <div class="col-lg-12">
                  <h1 class="video-slider-title pull-left">Vídeos</h1>
                </div>
              </div>


              <div class="row">
                <div class="col-lg-12 col-xs-12">

                  <?php

                    $paged = (get_query_var('paged')) ? get_query_var('paged') : 1;

                    query_posts('post_type=video&posts_per_page=6&paged='.$paged); //set your own query here

                    get_template_part( 'loop', 'grid' );

                    wp_reset_query();
                  ?>

                </div>
              </div>

            </div>
          </div>

      </div>
      </section>

<?php get_footer(); ?>
