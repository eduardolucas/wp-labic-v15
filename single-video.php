<?php get_header(); ?>

        <?php if (have_posts()) : while (have_posts()) : the_post(); ?>

          <header>
              <div class="container">

                <div class="row">
                  <div class="col-md-8 col-md-offset-2">

                    <div class="row">
                      <div class="col-lg-12">
                        <h1 class="pull-left video-label">Vídeos</h1>
                        <h2 class="pull-left text-uppercase video-title">
                          &nbsp; <?php the_title(); ?></h2>
                      </div>
                    </div>

                  </div>
                </div>

            </div>
          </header>

            <section class="video-section">
                <div class="video-section-embed container">
                    <iframe width="640" height="360" class="center-block" src="<?php echo get_post_meta($post->ID, 'wpcf-embed-url', TRUE); ?>" frameborder="0" allowfullscreen></iframe>
              </div>
            </section>

            <section class="video-slider">
              <div class="container">

                <div class="row">
                  <div class="col-md-8 col-md-offset-2">

                    <div class="row">
                      <div class="col-lg-12">
                        <?php the_content(); ?>
                      </div>
                    </div>

                    <div class="row">

                      <div class="col-lg-12">
                        <h2 class="pull-left">Compartilhe </h2>
                      </div>

                      <section class="row col-lg-12 clearfix">

                        <div class="text-center social-bar social-contacts">
                        <?php if ( function_exists( 'sharing_display' ) ) {
                            sharing_display( '', true );
                          }

                        if ( class_exists( 'Jetpack_Likes' ) ) {
                            $custom_likes = new Jetpack_Likes;
                            echo $custom_likes->post_likes( '' );
                          }
                        ?>
                        </div>

                      </section>
                    </div>

                  </div>
                </div>

              </div>
            </section>


            <section class="comments-section">
              <div class="container">

                <div class="row">
                  <div class="col-md-8 col-md-offset-2">

                    <div class="clearfix">
                      <h2 class="pull-left">Comentários</h2>
                    </div>

                    <?php comments_template(); ?>

                  </div>
                </div>

              </div>
            </section>

        <?php endwhile; ?>
        <?php else : ?>
        <?php endif; ?>

    </section>


<?php get_footer(); ?>
