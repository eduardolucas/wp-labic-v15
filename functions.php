<?php
/*
 *  Author: Labic | @ufeslabic
 *  URL: labic.net
 *  Custom functions.
 */

 // Register Custom Navigation Walker
 require_once('wp_bootstrap_navwalker.php');

 // Load Labic v15 scripts

 function labicv15_header_scripts()
 {
     if ($GLOBALS['pagenow'] != 'wp-login.php' && !is_admin()) {

         wp_register_script('classie-js', get_template_directory_uri() . '/js/classie.js', array(), ''); // Custom scripts
         wp_enqueue_script('classie-js'); // Enqueue it!

         wp_register_script('uisearch-js', get_template_directory_uri() . '/js/uisearch.js', array(), ''); // Custom scripts
         wp_enqueue_script('uisearch-js'); // Enqueue it!

     	  wp_register_script('modernizr', get_template_directory_uri() . '/js/modernizr-2.8.3-respond-1.4.2.min.js', array(), '2.8.3'); // Modernizr
         wp_enqueue_script('modernizr'); // Enqueue it!

         wp_register_script('bootstrap-js', get_template_directory_uri() . '/js/bootstrap.min.js', array('jquery'), ''); // Custom scripts
         wp_enqueue_script('bootstrap-js'); // Enqueue it!

         wp_enqueue_script( 'jquery' );
     }
 }

 // Load Labic v15 styles

function labicv15_styles()
{
    wp_register_style('bootstrap', get_template_directory_uri() . '/css/bootstrap.min.css', array(), '', 'all');
    wp_enqueue_style('bootstrap');

    wp_register_style('labicv15', get_template_directory_uri() . '/style.css', array(), '', 'all');
    wp_enqueue_style('labicv15');

    wp_register_style('fileicon', get_template_directory_uri() . '/css/fileicon.css', array(), '', 'all');
    wp_enqueue_style('fileicon');

}

if ( function_exists( 'add_theme_support' ) )
{
    // Add Menu Support
    add_theme_support('menus');

  	add_theme_support( 'post-thumbnails' );
	// set_post_thumbnail_size( 406, 281, true );
  add_image_size('large', 700, '', true); // Large Thumbnail
  add_image_size('medium', 250, '', true); // Medium Thumbnail
  add_image_size('small', 120, '', true); // Small Thumbnail
  add_image_size('home-thumb', 150, '', true); //width, height
	add_image_size('publ-thumb', 64, 64, true); //width, height
  add_image_size('video-thumb', 217, '', true); //width, height
}

// Bootstrap 3 wp menu


register_nav_menus( array(
    'primary' => __( 'Primary Menu', 'labicv15' ),
) );


function jptweak_remove_share() {
    remove_filter( 'the_content', 'sharing_display',19 );
    remove_filter( 'the_excerpt', 'sharing_display',19 );
    if ( class_exists( 'Jetpack_Likes' ) ) {
        remove_filter( 'the_content', array( Jetpack_Likes::init(), 'post_likes' ), 30, 1 );
    }
}


function wpcom_liveblog_get_output() {
  
}

// Search Form
function labicv15_wpsearch($form) {
	$form = '<form role="search" method="get" id="searchform" action="' . home_url( '/' ) . '" >
	<label class="screen-reader-text" for="s">' . __('', 'labicv15') . '</label>
	<input type="text" value="' . get_search_query() . '" name="s" id="s" placeholder="'.esc_attr__('','labicv15').'" />
	<input type="submit" id="searchsubmit" value="'. esc_attr__('Search') .'" />
	</form>';
	return $form;
} // don't remove this bracket!


/*------------------------------------*\
	Actions + Filters + ShortCodes
\*------------------------------------*/

// Add Actions

add_action('init', 'labicv15_header_scripts');
add_action('wp_enqueue_scripts', 'labicv15_styles');
add_action( 'loop_start', 'jptweak_remove_share' );

?>
