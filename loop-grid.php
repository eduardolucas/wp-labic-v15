<?php /* Start the Loop */ ?>

<?php $num_cols = 3; // set the number of columns here

for ( $i=1 ; $i <= $num_cols; $i++ ) :
echo '<div id="col-' . $i . '" class="col col-lg-4 col-xs-2" >';
$counter = $num_cols + 1 - $i; ?>

<?php while (have_posts()) : the_post();
if( $counter%$num_cols == 0 ) :
// begin of core posts output ?>

	<article class="video-post">
		<div id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

			<a href="<?php the_permalink(); ?>">
        <?php the_post_thumbnail( 'video-thumb', array('class' => 'video-post-th')); ?>
      </a>

  		<p class="video-post-title">
        <a href="<?php the_permalink() ?>" rel="bookmark" title="<?php the_title_attribute(); ?>">
        <?php the_title(); ?>
        </a>
      </p>

		</div><!-- #post-<?php the_ID(); ?> -->
	</article>

<?php //end of posts output

endif; $counter++;
endwhile;
rewind_posts();
echo '</div>'; //closes the column div
endfor; ?>
